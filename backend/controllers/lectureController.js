import asyncHandler from "express-async-handler";
import Lecture from "../models/lectureModel.js";
import Course from "../models/courseModel.js";
import User from "../models/userModel.js";

// @desc    Fetch all lectures
// @route   GET /api/lectures
// @access  Public
const getLectures = asyncHandler(async (req, res) => {
  const lectures = await Lecture.find({}).populate("course instructor");
  res.json(lectures);
});

// @desc    Fetch single lecture
// @route   GET /api/lectures/:id
// @access  Public
const getLectureById = asyncHandler(async (req, res) => {
  const lecture = await Lecture.findById(req.params.id).populate(
    "course instructor"
  );

  if (lecture) {
    res.json(lecture);
  } else {
    res.status(404);
    throw new Error("Lecture not found");
  }
});

// @desc    Create a lecture
// @route   POST /api/lectures
// @access  Private/Admin
const createLecture = asyncHandler(async (req, res) => {
  const { date, courseId, instructorId } = req.body;

  const course = await Course.findById(courseId);
  const instructor = await User.findById(instructorId);

  if (!course || !instructor) {
    res.status(404).json({ message: "Course or instructor not found" });
    return;
  }

  // Check if there is any lecture scheduled for the same course on the same date
  const existingLecture = await Lecture.findOne({
    course: courseId,
    date,
  });

  if (existingLecture) {
    res.status(400).json({
      message:
        "Another lecture is already scheduled for this course on the same date",
    });
    return;
  }

  // Check if the instructor is already assigned to a lecture on the same date
  const existingInstructorLecture = await Lecture.findOne({
    instructor: instructorId,
    date,
  });

  if (existingInstructorLecture) {
    res.status(400).json({
      message: "Instructor is already assigned to a lecture on this date",
    });
    return;
  }

  const lecture = new Lecture({
    date,
    course: course._id,
    instructor: instructor._id,
    // Add other fields as needed
  });

  const createdLecture = await lecture.save();
  res.status(201).json(createdLecture);
});

// @desc    Update a lecture
// @route   PUT /api/lectures/:id
// @access  Private/Admin
const updateLecture = asyncHandler(async (req, res) => {
  const { date, courseId, instructorId } = req.body;

  const lecture = await Lecture.findById(req.params.id);

  if (lecture) {
    const course = await Course.findById(courseId);
    const instructor = await User.findById(instructorId);

    if (!course || !instructor) {
      res.status(404);
      throw new Error("Course or instructor not found");
    }

    lecture.date = date;
    lecture.course = course._id;
    lecture.instructor = instructor._id;
    // Update other fields as needed

    const updatedLecture = await lecture.save();
    res.json(updatedLecture);
  } else {
    res.status(404);
    throw new Error("Lecture not found");
  }
});

// @desc    Delete a lecture
// @route   DELETE /api/lectures/:id
// @access  Private/Admin
const deleteLecture = asyncHandler(async (req, res) => {
  const lecture = await Lecture.findById(req.params.id);

  if (lecture) {
    await lecture.deleteOne();
    res.json({ message: "Lecture removed" });
  } else {
    res.status(404);
    throw new Error("Lecture not found");
  }
});
const getMyLectures = asyncHandler(async (req, res) => {
  console.log(req.user._id);
  const lectures = await Lecture.find({ instructor: req.user._id }).populate(
    "course instructor"
  );
  res.json(lectures);
});

export {
  getLectures,
  getLectureById,
  createLecture,
  updateLecture,
  getMyLectures,
  deleteLecture,
};
