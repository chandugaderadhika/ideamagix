import mongoose from "mongoose";

const courseSchema = new mongoose.Schema({
  name: { type: String, required: true },
  level: { type: String },
  description: { type: String },
  image: { type: String },
  // Add other fields as needed
});

const Course = mongoose.model("Course", courseSchema);
export default Course;
