import mongoose from "mongoose";
// Define schema for lectures
const lectureSchema = new mongoose.Schema({
  date: { type: Date, required: true },
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Course",
    required: true,
  },
  instructor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  // Add other fields as needed
});

const Lecture = mongoose.model("Lecture", lectureSchema);
export default Lecture;
