import mongoose from "mongoose";
import bcrypt from "bcryptjs";

// Define schema for users
const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false,
  },
  isInstructor: { type: Boolean, required: true, default: true },
  assignedLectureDates: [{ type: Date }],
});

userSchema.methods.matchPassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// Create models
const User = mongoose.model("User", userSchema);
// const Course = mongoose.model("Course", courseSchema);
// const Lecture = mongoose.model("Lecture", lectureSchema);

// export { User, Course, Lecture };
export default User;
