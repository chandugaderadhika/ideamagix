import express from "express";
import asyncHandler from "express-async-handler";
import {
  getLectures,
  getLectureById,
  createLecture,
  updateLecture,
  deleteLecture,
  getMyLectures,
} from "../controllers/lectureController.js";
import { admin, protect } from "../middlewares/authMiddleware.js";

const router = express.Router();

router.route("/").get(getLectures).post(createLecture);
router.route("/mylectures").get(protect, getMyLectures);
router
  .route("/:id")
  .get(getLectureById)
  .put(updateLecture)
  .delete(deleteLecture);

export default router;
