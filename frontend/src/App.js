import { Flex } from "@chakra-ui/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import HomeScreen from "./screens/HomeScreen";
import LoginScreen from "./screens/LoginScreen";
import CourseListScreen from "./screens/CourseListScreen";
import RegisterScreen from "./screens/RegisterScreen";
import CreateCourseScreen from "./screens/CreateCourceScreen";
import UserEditScreen from "./screens/UserEditScreen";
import UserListScreen from "./screens/UserListScreen";
import CourseEditScreen from "./screens/CourseEditScreen";
import CreateLectureScreen from "./screens/CreateLecureScreen";
import LectureListScreen from "./screens/LectureListScreen";

const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <Flex
        as="main"
        mt="72px"
        direction="column"
        py="6"
        px="6"
        bgColor="gray.200"
      >
        <Routes>
          <Route path="/" element={<HomeScreen />} />

          <Route path="/login" element={<LoginScreen />} />
          <Route path="/register" element={<RegisterScreen />} />
          <Route path="/admin/createcourse" element={<CreateCourseScreen />} />
          <Route path="/admin/userlist" element={<UserListScreen />} />
          <Route path="/admin/user/:id/edit" element={<UserEditScreen />} />
          <Route path="/admin/course/:id/edit" element={<CourseEditScreen />} />
          <Route path="/admin/courselist" element={<CourseListScreen />} />
          <Route
            path="/admin/createlecture"
            element={<CreateLectureScreen />}
          />
          <Route path="/admin/lecturelist" element={<LectureListScreen />} />
        </Routes>
      </Flex>
      <Footer />
    </BrowserRouter>
  );
};

export default App;
