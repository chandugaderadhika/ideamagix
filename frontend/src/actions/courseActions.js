import axios from "axios";

import {
  COURSE_CREATE_FAIL,
  COURSE_CREATE_REQUEST,
  COURSE_CREATE_SUCCESS,
  COURSE_DELETE_FAIL,
  COURSE_DELETE_REQUEST,
  COURSE_DELETE_SUCCESS,
  COURSE_DETAILS_FAIL,
  COURSE_DETAILS_REQUEST,
  COURSE_DETAILS_SUCCESS,
  COURSE_LIST_FAIL,
  COURSE_LIST_REQUEST,
  COURSE_LIST_SUCCESS,
  COURSE_UPDATE_FAIL,
  COURSE_UPDATE_REQUEST,
  COURSE_UPDATE_SUCCESS,
} from "../constants/courseConstants";

export const listCourses = () => async (dispatch) => {
  try {
    dispatch({ type: COURSE_LIST_REQUEST });

    const { data } = await axios.get(`/api/courses`);

    dispatch({ type: COURSE_LIST_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: COURSE_LIST_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const listCourseDetails = (id) => async (dispatch) => {
  try {
    dispatch({ type: COURSE_DETAILS_REQUEST });

    const { data } = await axios.get(`/api/courses/${id}`);

    dispatch({ type: COURSE_DETAILS_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: COURSE_DETAILS_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const deleteCourse = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: COURSE_DELETE_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    await axios.delete(`/api/courses/${id}`, config);

    dispatch({ type: COURSE_DELETE_SUCCESS });
  } catch (err) {
    dispatch({
      type: COURSE_DELETE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const createCourse =
  (name, level, description, image) => async (dispatch, getState) => {
    try {
      dispatch({ type: COURSE_CREATE_REQUEST });

      const {
        userLogin: { userInfo },
      } = getState();
      console.log("name", name);
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
          "Content-Type": "application/json",
        },
      };

      const { data } = await axios.post(
        `/api/courses`,
        { name, level, description, image },
        config
      );

      dispatch({ type: COURSE_CREATE_SUCCESS, payload: data });
    } catch (err) {
      dispatch({
        type: COURSE_CREATE_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
    }
  };

export const updateCourse = (product) => async (dispatch, getState) => {
  try {
    dispatch({ type: COURSE_UPDATE_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
        "Content-Type": "application/json",
      },
    };

    const { data } = await axios.put(
      `/api/courses/${product._id}`,
      product,
      config
    );

    dispatch({ type: COURSE_UPDATE_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: COURSE_UPDATE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};
