import axios from "axios";
import {
  LECTURE_CREATE_FAIL,
  LECTURE_CREATE_REQUEST,
  LECTURE_CREATE_SUCCESS,
  LECTURE_DELETE_FAIL,
  LECTURE_DELETE_REQUEST,
  LECTURE_DELETE_SUCCESS,
  LECTURE_DETAILS_FAIL,
  LECTURE_DETAILS_REQUEST,
  LECTURE_DETAILS_SUCCESS,
  LECTURE_LIST_FAIL,
  LECTURE_LIST_REQUEST,
  LECTURE_LIST_SUCCESS,
  LECTURE_MY_LIST_FAIL,
  LECTURE_MY_LIST_REQUEST,
  LECTURE_MY_LIST_SUCCESS,
  LECTURE_PAY_FAIL,
  LECTURE_PAY_REQUEST,
  LECTURE_PAY_SUCCESS,
} from "../constants/lectureConstants";

export const createLecture =
  (date, courseId, instructorId) => async (dispatch, getState) => {
    try {
      dispatch({ type: LECTURE_CREATE_REQUEST });

      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
          "Content-Type": "application/json",
        },
      };

      const { data } = await axios.post(
        `/api/lectures`,
        { date, courseId, instructorId },
        config
      );

      dispatch({ type: LECTURE_CREATE_SUCCESS, payload: data });
    } catch (err) {
      dispatch({
        type: LECTURE_CREATE_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
    }
  };

export const getLectureDetails = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: LECTURE_DETAILS_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(`/api/lectures/${id}`, config);

    dispatch({ type: LECTURE_DETAILS_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: LECTURE_DETAILS_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const payLecture =
  (lectureId, paymentResult) => async (dispatch, getState) => {
    try {
      dispatch({ type: LECTURE_PAY_REQUEST });

      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
        },
      };

      const { data } = await axios.put(
        `/api/lectures/${lectureId}/pay`,
        paymentResult,
        config
      );

      dispatch({ type: LECTURE_PAY_SUCCESS, payload: data });
    } catch (err) {
      dispatch({
        type: LECTURE_PAY_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
    }
  };

export const listMyLectures = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LECTURE_MY_LIST_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(`/api/lectures/mylectures`, config);

    dispatch({ type: LECTURE_MY_LIST_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: LECTURE_MY_LIST_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const listLectures = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LECTURE_LIST_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(`/api/lectures`, config);

    dispatch({ type: LECTURE_LIST_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: LECTURE_LIST_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const deleteLecture = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: LECTURE_DELETE_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    await axios.delete(`/api/lectures/${id}`, config);

    dispatch({ type: LECTURE_DELETE_SUCCESS });
  } catch (err) {
    dispatch({
      type: LECTURE_DELETE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};
