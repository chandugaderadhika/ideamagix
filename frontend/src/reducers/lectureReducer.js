import {
  LECTURE_CREATE_FAIL,
  LECTURE_CREATE_REQUEST,
  LECTURE_CREATE_SUCCESS,
  LECTURE_DELETE_FAIL,
  LECTURE_DELETE_REQUEST,
  LECTURE_DELETE_SUCCESS,
  LECTURE_DETAILS_FAIL,
  LECTURE_DETAILS_REQUEST,
  LECTURE_DETAILS_RESET,
  LECTURE_DETAILS_SUCCESS,
  LECTURE_LIST_FAIL,
  LECTURE_LIST_REQUEST,
  LECTURE_LIST_RESET,
  LECTURE_LIST_SUCCESS,
  LECTURE_MY_LIST_FAIL,
  LECTURE_MY_LIST_REQUEST,
  LECTURE_MY_LIST_RESET,
  LECTURE_MY_LIST_SUCCESS,
  LECTURE_PAY_FAIL,
  LECTURE_PAY_REQUEST,
  LECTURE_PAY_RESET,
  LECTURE_PAY_SUCCESS,
  LECTURE_CREATE_RESET,
} from "../constants/lectureConstants";

export const lectureCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case LECTURE_CREATE_REQUEST:
      return { loading: true };
    case LECTURE_CREATE_SUCCESS:
      return { loading: false, success: true, lecture: action.payload };
    case LECTURE_CREATE_FAIL:
      return { loading: false, error: action.payload };
    case LECTURE_CREATE_RESET:
      return {};
    default:
      return state;
  }
};

export const lectureDetailsReducer = (
  state = {
    loading: true,
    lecture: {},
  },
  action
) => {
  switch (action.type) {
    case LECTURE_DETAILS_REQUEST:
      return { loading: true };
    case LECTURE_DETAILS_SUCCESS:
      return { loading: false, lecture: action.payload };
    case LECTURE_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case LECTURE_DETAILS_RESET:
      return {};
    default:
      return state;
  }
};

export const lecturePayReducer = (state = {}, action) => {
  switch (action.type) {
    case LECTURE_PAY_REQUEST:
      return { loading: true };
    case LECTURE_PAY_SUCCESS:
      return { loading: false, success: true };
    case LECTURE_PAY_FAIL:
      return { loading: false, error: action.payload };
    case LECTURE_PAY_RESET:
      return {};
    default:
      return state;
  }
};

export const lectureMyListReducer = (state = { lectures: [] }, action) => {
  switch (action.type) {
    case LECTURE_MY_LIST_REQUEST:
      return { loading: true };
    case LECTURE_MY_LIST_SUCCESS:
      return { loading: false, lectures: action.payload };
    case LECTURE_MY_LIST_FAIL:
      return { loading: false, error: action.payload };
    case LECTURE_MY_LIST_RESET:
      return { lectures: [] };
    default:
      return state;
  }
};

export const lectureListReducer = (state = { lectures: [] }, action) => {
  switch (action.type) {
    case LECTURE_LIST_REQUEST:
      return { loading: true };
    case LECTURE_LIST_SUCCESS:
      return { loading: false, lectures: action.payload };
    case LECTURE_LIST_FAIL:
      return { loading: false, error: action.payload };
    case LECTURE_LIST_RESET:
      return { lectures: [] };
    default:
      return state;
  }
};

export const lectureDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case LECTURE_DELETE_REQUEST:
      return { loading: true };
    case LECTURE_DELETE_SUCCESS:
      return { loading: false, success: true };
    case LECTURE_DELETE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
