import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
  Select,
} from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
import { createCourse } from "../actions/courseActions";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { COURSE_CREATE_RESET } from "../constants/courseConstants";

const CreateCourseScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { id: productId } = useParams();

  const [name, setName] = useState("");

  const [image, setImage] = useState("");
  const [level, setLevel] = useState("");
  const [description, setDescription] = useState("");
  const [message, setMessage] = useState(null);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const courseCreate = useSelector((state) => state.courseCreate);
  const { loading, error, course } = courseCreate;

  useEffect(() => {
    if (course) {
      dispatch({ type: COURSE_CREATE_RESET });

      navigate("/admin/courselist");
    } else if (!userInfo && !userInfo.isAdmin) {
      navigate("/login");
    }
  }, [dispatch, navigate, userInfo, course]);

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(createCourse(name, level, description, image));
  };

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("image", file);

    try {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const { data } = await axios.post(`/api/uploads`, formData, config);
      setImage(data);
    } catch (err) {
      console.error(err);
    }
  };

  const options = [
    { value: "beginner", label: "Beginner" },
    { value: "intermediate", label: "Intermediate" },
    { value: "advanced", label: "Advanced" },
    { value: "expert", label: "Expert" },
  ];

  return (
    <>
      <Link as={RouterLink} to="/admin/courselist">
        Go Back
      </Link>

      <Flex w="full" alignItems="center" justifyContent="center" py="5">
        <FormContainer>
          <Heading as="h1" mb="8" fontSize="3xl">
            Create Course
          </Heading>
          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}

          <form onSubmit={submitHandler}>
            {/* NAME */}
            <FormControl id="name" isRequired>
              <FormLabel>Course Name</FormLabel>
              <Input
                type="text"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>
            <Spacer h="3" />

            {/* IMAGE */}
            <FormControl id="image" isRequired>
              <FormLabel>Image</FormLabel>
              <Input
                type="text"
                placeholder="Enter image url"
                value={image}
                onChange={(e) => setImage(e.target.value)}
              />
              <Input type="file" onChange={uploadFileHandler} />
            </FormControl>
            <Spacer h="3" />

            {/* DESCRIPTION */}
            <FormControl id="description" isRequired>
              <FormLabel>Description</FormLabel>
              <Input
                type="text"
                placeholder="Enter description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </FormControl>
            <Spacer h="3" />

            {/* Level */}
            <FormControl id="category" isRequired>
              <FormLabel>Level</FormLabel>
              <Select
                placeholder="Select Level"
                value={level} // Use the state variable for the selected value
                onChange={(e) => setLevel(e.target.value)} // Update the state variable on change
              >
                {options.map((option) => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </Select>
            </FormControl>
            <Spacer h="3" />

            <Spacer h="3" />

            <Button type="submit" isLoading={loading} colorScheme="teal" mt="4">
              Create
            </Button>
          </form>
        </FormContainer>
      </Flex>
    </>
  );
};

export default CreateCourseScreen;
