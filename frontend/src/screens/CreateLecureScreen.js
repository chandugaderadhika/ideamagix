import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
  Select,
} from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
import { createLecture } from "../actions/lectureActions";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { LECTURE_CREATE_RESET } from "../constants/lectureConstants";
import { listUsers } from "../actions/userActions";
import { listCourses } from "../actions/courseActions";
const CreateLectureScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  //   const { id: courseId } = useParams();

  const [date, setDate] = useState("");
  const [instructorId, setInstructorId] = useState("");
  const [message, setMessage] = useState(null);
  const [courseId, setCourseId] = useState("");
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const lectureCreate = useSelector((state) => state.lectureCreate);
  const { loading, error, lecture } = lectureCreate;

  const courseList = useSelector((state) => state.courseList);
  const { courses } = courseList;
  const userList = useSelector((state) => state.userList);
  const { users } = userList;

  useEffect(() => {
    if (lecture) {
      dispatch({ type: LECTURE_CREATE_RESET });

      navigate(`/admin/lecturelist`);
    } else if (!userInfo && !userInfo.isAdmin) {
      navigate("/login");
    } else {
      dispatch(listCourses());
      dispatch(listUsers());
    }
  }, [dispatch, navigate, userInfo, lecture]);

  const submitHandler = (e) => {
    e.preventDefault();
    console.log(date, courseId, instructorId);
    dispatch(createLecture(date, courseId, instructorId));
  };
  //   courses.map((user) => {
  //     console.log(user.name);
  //   });

  return (
    <>
      <Link as={RouterLink} to={`/admin/lecturelist`}>
        Go Back
      </Link>

      <Flex w="full" alignItems="center" justifyContent="center" py="5">
        <FormContainer>
          <Heading as="h1" mb="8" fontSize="3xl">
            Create Lecture
          </Heading>
          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}

          <form onSubmit={submitHandler}>
            {/* DATE */}
            <FormControl id="date" isRequired>
              <FormLabel>Date</FormLabel>
              <Input
                type="date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
            </FormControl>
            <Spacer h="3" />

            {/* INSTRUCTOR */}
            <FormControl id="instructor" isRequired>
              <FormLabel>Instructor</FormLabel>
              <Select
                value={instructorId}
                onChange={(e) => setInstructorId(e.target.value)}
              >
                <option value="">Select an instructor</option>
                {users.map((user) => (
                  <option key={user._id} value={user._id}>
                    {user.name}
                  </option>
                ))}
              </Select>
            </FormControl>
            <Spacer h="3" />
            <FormControl id="instructor" isRequired>
              <FormLabel>Course</FormLabel>
              <Select
                value={courseId}
                onChange={(e) => setCourseId(e.target.value)}
              >
                <option value="">Select a course</option>
                {courses.map((course) => (
                  <option key={course._id} value={course._id}>
                    {course.name}
                  </option>
                ))}
              </Select>{" "}
            </FormControl>
            <Spacer h="3" />

            <Button type="submit" isLoading={loading} colorScheme="teal" mt="4">
              Create
            </Button>
          </form>
        </FormContainer>
      </Flex>
    </>
  );
};

export default CreateLectureScreen;
