import {
  Box,
  Button,
  Flex,
  Heading,
  Icon,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Link as RouterLink, useNavigate } from "react-router-dom";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { listMyLectures } from "../actions/lectureActions";
import { getUserDetails } from "../actions/userActions";
const HomeScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  //   useEffect(() => {
  //     dispatch(listProducts());
  //   }, [dispatch]);
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const lectureMyList = useSelector((state) => state.lectureMyList);
  const {
    loading: loadingLectures,
    error: errorLectures,
    lectures,
  } = lectureMyList;

  useEffect(() => {
    if (!userInfo) {
      navigate("/login");
    } else {
      // dispatch(getUserDetails());
      dispatch(listMyLectures());
    }
  }, [dispatch, navigate, userInfo]);

  return (
    <>
      <Heading as="h2" mb="8" fontSize="xl">
        My lectures
      </Heading>
      {loadingLectures ? (
        <Loader />
      ) : errorLectures ? (
        <Message type="error">{errorLectures}</Message>
      ) : (
        <>
          {lectures.length === 0 ? (
            <Message type="info">No lectures found.</Message>
          ) : (
            <Box bgColor="white" rounded="lg" shadow="lg" px="5" py="5">
              <Table variant="striped" colorScheme="gray" size="sm">
                <Thead>
                  <Tr>
                    <Th>ID</Th>
                    <Th>LECTURE DATE</Th>
                    <Th>COURSE NAME</Th>
                    <Th>INSTRUCTOR</Th>
                    <Th></Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {lectures.map((lecture) => (
                    <Tr key={lecture._id}>
                      <Td>{lecture._id}</Td>
                      <Td>{lecture.date.substring(0, 10)}</Td>
                      <Td>{lecture.course?.name}</Td>
                      <Td>{lecture.instructor?.name}</Td>
                      <Td></Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
            </Box>
          )}
        </>
      )}
    </>
  );
};

export default HomeScreen;
