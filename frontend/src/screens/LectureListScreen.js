import {
  Box,
  Button,
  Flex,
  Heading,
  Icon,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { IoAdd, IoPencilSharp, IoTrashBinSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import {
  createLecture,
  deleteLecture,
  listLectures,
} from "../actions/lectureActions";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { LECTURE_CREATE_RESET } from "../constants/lectureConstants";

const LectureListScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const lectureList = useSelector((state) => state.lectureList);
  const { loading, error, lectures } = lectureList;

  const lectureDelete = useSelector((state) => state.lectureDelete);
  const {
    loading: loadingDelete,
    error: errorDelete,
    success: successDelete,
  } = lectureDelete;

  const lectureCreate = useSelector((state) => state.lectureCreate);
  const {
    loading: loadingCreate,
    error: errorCreate,
    success: successCreate,
    lecture: createdLecture,
  } = lectureCreate;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    dispatch({ type: LECTURE_CREATE_RESET });

    if (!userInfo.isAdmin) {
      navigate("/login");
    }

    if (successCreate) {
      navigate(`/admin/lecture/${createdLecture._id}/edit`);
    } else {
      dispatch(listLectures());
    }
  }, [
    dispatch,
    navigate,
    userInfo,
    successDelete,
    successCreate,
    createdLecture,
  ]);

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure")) {
      dispatch(deleteLecture(id));
    }
  };

  return (
    <>
      <Flex mb="5" alignItems="center" justifyContent="space-between">
        <Heading as="h1" fontSize="3xl" mb="5">
          Lecture
        </Heading>
        <Button as={RouterLink} to="/admin/createlecture" colorScheme="teal">
          <Icon as={IoAdd} mr="2" fontSize="xl" fontWeight="bold" /> Create
          Lecture
        </Button>
      </Flex>

      {loadingDelete && <Loader />}
      {errorDelete && <Message type="error">{errorDelete}</Message>}
      {loadingCreate && <Loader />}
      {errorCreate && <Message type="error">{errorCreate}</Message>}

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (
        <Box bgColor="white" rounded="lg" shadow="lg" px="5" py="5">
          <Table variant="striped" colorScheme="gray" size="sm">
            <Thead>
              <Tr>
                <Th>ID</Th>
                <Th>LECTURE DATE</Th>
                <Th>COURSE NAME</Th>
                <Th>INSTRUCTOR</Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {lectures.map((lecture) => (
                <Tr key={lecture._id}>
                  <Td>{lecture._id}</Td>
                  <Td>{lecture.date.substring(0, 10)}</Td>
                  <Td>{lecture.course?.name}</Td>
                  <Td>{lecture.instructor?.name}</Td>
                  <Td>
                    <Flex justifyContent="flex-end" alignItems="center">
                      {/* <Button
                        mr="4"
                        as={RouterLink}
                        to={`/admin/lecture/${lecture._id}/edit`}
                        colorScheme="teal"
                      >
                        <Icon as={IoPencilSharp} color="white" size="sm" />
                      </Button> */}
                      <Button
                        mr="4"
                        colorScheme="red"
                        onClick={() => deleteHandler(lecture._id)}
                      >
                        <Icon as={IoTrashBinSharp} color="white" size="sm" />
                      </Button>
                    </Flex>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </Box>
      )}
    </>
  );
};

export default LectureListScreen;
